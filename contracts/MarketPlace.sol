pragma solidity >=0.5.0 <= 0.6.0;
pragma experimental ABIEncoderV2;

import "./SafeMath.sol";

contract MarketPlace {

    using SafeMath for uint256;

    enum Etat {OUVERT, ENCOURS, FERMEE}
    enum Type {ILLUSTRATEUR, DEMANDEUR}
    enum Satisfaction{NC, MAUVAIS,CORRECT, BON, TRES_BON}

    event NewInscription(User user);
    event NewDemand(Demand demand);

    struct User {
        address _address;
        uint256 _reputation;
        string _nom;
        string _email;
        string _societe;
        Type _type;
        string[] _commentaires;
        Satisfaction _satisfaction;
    }

    struct Demand {
        address demandeur;
        bytes32 urlHash;
        string titre;
        string description;
        uint256 remuneration;
        uint256 delai;
        uint256 minRepWanted;
        uint256 dateRemise;
        Etat etat;
        address[] candidats;
    }

    address private admin;
    uint256 private repMin;

    mapping(address => User) private users;
    mapping(address => bool) private bannedUsers;
    mapping(address => Demand) private demandes;

    User[] usersList;
    Demand[] demandsList;

    modifier isAdmin() {
        if (msg.sender == admin) _;
    }

    modifier isUser() {
        require(users[msg.sender]._address == msg.sender,"Vous devez être inscrit !");
        _;
    }

    constructor(uint256 _repMin) public{
        admin = msg.sender;
        repMin = _repMin;
    }

    function inscription(string calldata _name, string calldata _societe, string calldata _email, Type _type) external {
        require(bannedUsers[msg.sender] == false);
        User memory newUser = User(msg.sender, 1, _name, _email, _societe, _type, new string[](0), Satisfaction.NC);
        users[msg.sender] = newUser;
        usersList.push(newUser);
    }

    function banUser(address _bannedAddress) public isAdmin {
        bannedUsers[_bannedAddress] = true;
        users[_bannedAddress]._reputation = 0;
    }

    function ajouterDemande(string calldata _titre, string calldata _url, uint256 _remuneration, uint256 _delai, string calldata _description, uint256 _minRep) external payable isUser {
        uint256 remuneration = _remuneration.add(_remuneration.mul(2).div(100));
        bytes32 url = keccak256(bytes(_url));
        Demand memory newDemand = Demand(msg.sender,url, _titre, _description, remuneration, _delai, _minRep, 0,  Etat.OUVERT,new address[](0));

        demandes[msg.sender] = newDemand;
        demandsList.push(newDemand);
    }

    /**** Contractualisation ****/
    function postuler() external {

    }

    function accepterOffre() external {

    }

    function livraison() external {

    }

    function toLate() external {

    }


}
